/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package residencia1;
import residencia1.Banheiro;
import residencia1.Corredor;
import residencia1.Cozinha;
import residencia1.Garagem;
import residencia1.Quarto1;
import residencia1.Quarto2;
import residencia1.SalaEstar;
import residencia1.SalaJantar;
/**
 *
 * @author filipi
 */
public class Residencia1 {

    /**
     * @param args the command line arguments
     */
    
    Banheiro banheiro= new Banheiro();
    Corredor corredor=new Corredor();
    Cozinha cozinha=new Cozinha();
    Garagem garagem=new Garagem();
    Quarto1 quarto1=new Quarto1();
    Quarto2 quarto2=new Quarto2();
    SalaEstar salaestar=new SalaEstar();
    SalaJantar salajantar=new SalaJantar();
    
    public String situacaob;
    public String situacaoc1;
    public String situacaoc2;
    public String situacaoco;
    public String situacaog1;
    public String situacaog2;
    public String situacaoq1;
    public String situacaoq2;
    public String situacaose;
    public String situacaosj;
    
    public Residencia1(){
    
        super();
        situacaob="";
        situacaoc1="";
        situacaoc2="";
        situacaoco="";
        situacaog1="";
        situacaog2="";
        situacaoq1="";
        situacaoq2="";
        situacaose="";
        situacaosj="";
    
    }
    public String getSituacaob(){
            return this.situacaob;
        }
    public void setSituacaob(String situacaob){
        this.situacaob=situacaob;
    }
    
    public String getSituacaoc1(){
            return this.situacaoc1;
        }
    public void setSituacaoc1(String situacaoc1){
        this.situacaoc1=situacaoc1;
    }
    public String getSituacaoc2(){
            return this.situacaoc2;
        }
    public void setSituacaoc2(String situacaoc2){
        this.situacaoc2=situacaoc2;
    }
    public String getSituacaoco(){
            return this.situacaoco;
        }
    public void setSituacaoco(String situacaoco){
        this.situacaoco=situacaoco;
    }
    public String getSituacaog1(){
            return this.situacaog1;
        }
    public void setSituacaog1(String situacaog1){
        this.situacaog1=situacaog1;
    }
    public String getSituacaog2(){
            return this.situacaog2;
        }
    public void setSituacaog2(String situacaog2){
        this.situacaog2=situacaog2;
    }
    public String getSituacaoq1(){
            return this.situacaoq1;
        }
    public void setSituacaoq1(String situacaoq1){
        this.situacaoq1=situacaoq1;
    }
    public String getSituacaoq2(){
            return this.situacaoq2;
        }
    public void setSituacaoq2(String situacaoq2){
        this.situacaoq2=situacaoq2;
    }
    public String getSituacaose(){
            return this.situacaose;
        }
    public void setSituacaose(String situacaose){
        this.situacaose=situacaose;
    }
    public String getSituacaosj(){
            return this.situacaosj;
        }
    public void setSituacaosj(String situacaosj){
        this.situacaosj=situacaosj;
    }
    public void loop(){
    if(situacaob.equals("ON")){
        banheiro.LigarLB();
    
}else{
        banheiro.DesligarLB();
    }
    if(situacaoc1.equals("ON")){
        corredor.LigarLC1();
    }else{
        corredor.DesligarLC1();
    }
    if(situacaoc2.equals("ON")){
        corredor.LigarLC2();
    }else{
        corredor.DesligarLC2();
    }
    if(situacaoco.equals("ON")){
        cozinha.LigarLCo();
    }else{
        cozinha.DesligarLCo();
    }
    if(situacaog1.equals("ON")){
        garagem.LigarLG1();
    }else{
        garagem.DesligarLG1();
    }
    if(situacaog2.equals("ON")){
        garagem.LigarLG2();
    }else{
        garagem.DesligarLG2();
    }
    if(situacaoq1.equals("ON")){
        quarto1.LigarLQ1();
    }else{
        quarto1.DesligarLQ1();
    }
    if(situacaoq2.equals("ON")){
        quarto2.LigarLQ2();
    }else{
        quarto2.DesligarLQ2();
    }
    if(situacaose.equals("ON")){
        salaestar.LigarLE();
    }else{
        salaestar.DesligarLE();
    }
    if(situacaosj.equals("ON")){
        salajantar.LigarLJ();
    }else{
        salajantar.DesligarLJ();
    }
    }
    
    
}
